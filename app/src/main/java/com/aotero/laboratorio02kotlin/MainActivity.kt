package com.aotero.laboratorio02kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //1. Evento onclick del boton
        btnVerificar.setOnClickListener {

            //2. Recuperar el input del año de nacimiento
            val anioNacimiento = edtNacimiento.text.toString().toInt()

            //when
            val respuesta = when(anioNacimiento){

                in 1930..1948 ->{
                    "Silent Generation"
                }
                in 1949..1968 ->{
                    "Baby Boom"
                }
                in 1969..1980 ->{
                    "Generacion X"
                }
                in 1981..1993 ->{
                    "Generacion Y"
                }
                in 1994..2010 ->{
                    "Generacion Z"
                }
                else->"No pertenece a ninguna generacion"
            }
            tvResultadoGeneracion.text=respuesta

            val poblacion = when(anioNacimiento){

                in 1930..1948 ->{
                    "6.300.000"
                }
                in 1949..1968 ->{
                    "12.200.000"
                }
                in 1969..1980 ->{
                    "9.300.000"
                }
                in 1981..1993 ->{
                    "7.200.000"
                }
                in 1994..2010 ->{
                    "7.800.000"
                }
                else->"No pertenece a ninguna generacion"
            }
            tvPoblacion.text=poblacion

             when(anioNacimiento){

                in 1930..1948 ->{
                    imgRasgo.setImageResource(R.drawable.austeridad);
                }
                in 1949..1968 ->{
                    imgRasgo.setImageResource(R.drawable.ambicion);
                }
                in 1969..1980 ->{
                    imgRasgo.setImageResource(R.drawable.obsesion);
                }
                in 1981..1993 ->{
                    imgRasgo.setImageResource(R.drawable.frustracion);
                }
                in 1994..2010 ->{
                    imgRasgo.setImageResource(R.drawable.irrelevancia);
                }
                else->imgRasgo.setImageResource(R.drawable.nada);
            }

        }
    }
}